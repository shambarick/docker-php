#!/bin/bash

function set_memory_limit {
    FOLDER=$1
    VALUE=$2
    sed -i "s/memory_limit =.*/memory_limit = ${VALUE}/" /etc/php/7.0/${FOLDER}/php.ini
}

function set_timezone {
    FOLDER=$1
    VALUE=$2
    sed -i "s/;date\.timezone =.*/date.timezone = ${VALUE}/" /etc/php/7.0/${FOLDER}/php.ini
}

PHP_TIMEZONE="Pacific\/Tahiti"

if [ ls /etc/apache2 2>/dev/null ]; then
    set_memory_limit "apache2" "200M"
    set_timezone "apache2" $PHP_TIMEZONE
fi

set_memory_limit "cli" "200M"
set_timezone "cli" $PHP_TIMEZONE
