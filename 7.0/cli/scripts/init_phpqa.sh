#!/bin/bash

composer global require "phpmd/phpmd" \
                        "sebastian/phpcpd" \
                        "squizlabs/php_codesniffer" \
                        "sensiolabs/security-checker" \
                        "phpunit/phpunit"
